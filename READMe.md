
#########INTRODUCTION##################
We have used seleniun, java and TestNG to automate web application.
It is a maven project where we fetched all dependancies from pom.xml file.
We have created POM where all webelement stored in src/main/java package.
We have config file from where we can read value like browser name, driver path etc. and present in src/main/resoures
We have test script written under package src/test/java


#############EXECUTION########################
We can excecute project using testng.xml file present in project.


###############REPORTING#####################
We can generate allure report after execution using command "allure serve C:\Users\Nikhil\eclipse-workspace\SeleniumProject\allure-results"
Mentioned path is allure result path which automatically gets generate.