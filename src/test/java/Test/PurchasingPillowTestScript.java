package Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import SeleniumObjectRepo.DebitOrCreditCardDetailsPage;
import SeleniumObjectRepo.HomePage;
import SeleniumObjectRepo.OTPPage;
import SeleniumObjectRepo.OrderSummaryPage;
import SeleniumObjectRepo.SelectPaymentPage;
import Utility.Utility;


public class PurchasingPillowTestScript {
	public static WebDriver driver;
	Properties prop;
	//allure serve C:\Users\Nikhil\eclipse-workspace\SeleniumProject\allure-results
	
	@BeforeMethod
	public void runConfigaration() throws IOException {
		
		 prop=new Properties();
		FileInputStream ip=new FileInputStream("C:\\Users\\Nikhil\\eclipse-workspace\\SeleniumProject\\src\\main\\resources\\config.properties");
		prop.load(ip);
		String browserName=prop.getProperty("browser");
		
		if(browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",prop.getProperty("driverPath"));
			 driver = new ChromeDriver();
		}
		else if(browserName.equals("FF")) {
			System.setProperty("webdriver.chrome.driver",prop.getProperty("driverPath"));
			 driver = new FirefoxDriver();
		}
		
		else if(browserName.equals("IE")) {
			System.setProperty("webdriver.chrome.driver",prop.getProperty("driverPath"));
			 driver = new InternetExplorerDriver();
		}
		else {
			System.out.println("No Browser value is given");
		}
		
		driver.get(prop.getProperty("url"));
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.MILLISECONDS);
		driver.manage().window().maximize();
		
	}
	
	
	@Test (priority = 1)
		public void successCreditCardPayment() {
		
		HomePage homepage=new HomePage(driver);
		homepage.clickOnBuyBtn();
		try {Thread.sleep(2000);} catch (InterruptedException e1) {}
		homepage.clickOnCheckOutBtn();
		
		OrderSummaryPage orderSummaryPage= new OrderSummaryPage(driver);
		homepage.WaitForElementToLoad();
		orderSummaryPage.switchToFrame();
		orderSummaryPage.verifyContinueBtn();
		orderSummaryPage.clickOnContinueBtn();
		
		SelectPaymentPage selectPaymentPage= new SelectPaymentPage(driver);
		selectPaymentPage.clickOnCreditCardLink();
		
		DebitOrCreditCardDetailsPage debitOrCreditCardDetailsPage= new DebitOrCreditCardDetailsPage(driver);
		homepage.WaitForElementToLoad();
		debitOrCreditCardDetailsPage.verifyPayNowBtn();
		debitOrCreditCardDetailsPage.enterCardNumber(prop.getProperty("creditCardNumber"));
		debitOrCreditCardDetailsPage.enterExpiryDate(prop.getProperty("expiryDate"));
		debitOrCreditCardDetailsPage.enterCVV(prop.getProperty("CVV"));
		debitOrCreditCardDetailsPage.clickOnPayNowBtn();
		
		OTPPage otpPage= new OTPPage(driver);
		try{Thread.sleep(10000);} catch (InterruptedException e1) {}
		otpPage.switchToFrame();
		otpPage.verifyOkBtn();
		otpPage.enterPassword(prop.getProperty("OTPPass"));
		otpPage.clickOnOkBtn();
		
		homepage.WaitForElementToLoad();

		try{homepage.verifyBuyBtn();}catch(WebDriverException e) {}
		
		driver.quit();
		

	}
	
	  @Test(priority = 2)
	  public void failedCreditCardPayment() { 
		  
			HomePage homepage=new HomePage(driver);
			homepage.clickOnBuyBtn();
			try {Thread.sleep(2000);} catch (InterruptedException e1) {}
			homepage.clickOnCheckOutBtn();
			
			OrderSummaryPage orderSummaryPage= new OrderSummaryPage(driver);
			homepage.WaitForElementToLoad();
			orderSummaryPage.switchToFrame();
			orderSummaryPage.verifyContinueBtn();
			orderSummaryPage.clickOnContinueBtn();
			
			SelectPaymentPage selectPaymentPage= new SelectPaymentPage(driver);
			selectPaymentPage.clickOnCreditCardLink();
			
			DebitOrCreditCardDetailsPage debitOrCreditCardDetailsPage= new DebitOrCreditCardDetailsPage(driver);
			homepage.WaitForElementToLoad();
			debitOrCreditCardDetailsPage.verifyPayNowBtn();
			debitOrCreditCardDetailsPage.enterCardNumber(prop.getProperty("creditCardNumber"));
			debitOrCreditCardDetailsPage.enterExpiryDate(prop.getProperty("expiryDate"));
			debitOrCreditCardDetailsPage.enterCVV(prop.getProperty("CVV"));
			debitOrCreditCardDetailsPage.clickOnPayNowBtn();
			
			OTPPage otpPage= new OTPPage(driver);
			try{Thread.sleep(10000);} catch (InterruptedException e1) {}
			otpPage.switchToFrame();
			otpPage.verifyOkBtn();
			otpPage.enterPassword(prop.getProperty("OTPFail"));
			otpPage.clickOnOkBtn();
			
			homepage.WaitForElementToLoad();
			try{otpPage.verifyTransactionFailedText();}catch(WebDriverException e) {}
			
			driver.quit();
		  
	  }
	 

}
