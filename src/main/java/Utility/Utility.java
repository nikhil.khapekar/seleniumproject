package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {
	XSSFWorkbook wb;
	XSSFSheet sheet1;
	
	
	public Utility() {
		
		try {
			File src=new File("C:\\Users\\Nikhil\\Desktop\\TestData.xslx");
			FileInputStream  fis=new FileInputStream(src);
			 wb =new XSSFWorkbook(fis);
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getData(int sheetNumber, int row, int column) {
		 sheet1=wb.getSheetAt(sheetNumber);
		 
		 String data=sheet1.getRow(row).getCell(column).getStringCellValue();
		 return data;
		 
	}

}
