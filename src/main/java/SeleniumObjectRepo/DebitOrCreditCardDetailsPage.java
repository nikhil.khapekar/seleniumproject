package SeleniumObjectRepo;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DebitOrCreditCardDetailsPage {
	
	WebDriver driver;


	By txtFieldCardNumber = By.name("cardnumber");
    By txtFieldExpiryDate=By.xpath("//input[@placeholder='MM / YY']");
    By btnPayNow=By.xpath("//a[contains(@class,'button-main-content')]");
    By txtFieldCVV=By.xpath("//input[@inputmode='numeric']");
    
  
  
    
    


    public DebitOrCreditCardDetailsPage(WebDriver driver){

        this.driver = driver;

    }

    public void verifyPayNowBtn() {
    	assertEquals(driver.findElement(btnPayNow).isDisplayed(), true, "check Pay Now button");
    }
    
    public void enterCardNumber(String cardNumber) {
    	driver.findElement(txtFieldCardNumber).sendKeys(cardNumber);
    }
    
    
    public void enterExpiryDate(String expiryDate) {
    	driver.findElement(txtFieldExpiryDate).sendKeys(expiryDate);
    }
    
    public void enterCVV(String CVV) {
    	driver.findElement(txtFieldCVV).sendKeys(CVV);
    }
    
    public void clickOnPayNowBtn() {
    	driver.findElement(btnPayNow).click();
    }

}
