package SeleniumObjectRepo;

import static org.testng.Assert.assertEquals;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OTPPage {
	
	
	WebDriver driver;

    By txtBoxPassword = By.xpath("//input[@type='password']");

    By btnOk = By.name("ok");
    
    By txtTransactionFailed = By.xpath("//span[text()='Transaction failed']");
 //   By btnBuy = By.xpath("//a[text()='BUY NOW']");
   
    
  


    public OTPPage(WebDriver driver){

        this.driver = driver;

    }
     
    
    public void switchToFrame() {
    	driver.switchTo().frame(0);
    }
   
    public void verifyTransactionFailedText() {
        //	new WebDriverWait(driver, 30L).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(btnCheckOut));
        	assertEquals(driver.findElement(txtTransactionFailed).isDisplayed(), true, "check Transaction Failed Text");
        }
    
    
    public void verifyOkBtn() {
    //	new WebDriverWait(driver, 30L).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(btnCheckOut));
    	assertEquals(driver.findElement(btnOk).isDisplayed(), true, "check CheckOut Button");
    }

    public void enterPassword(String password) {
    	driver.findElement(txtBoxPassword).sendKeys(password);
    }
  
    
    public void clickOnOkBtn() {
    	driver.findElement(btnOk).click();
    }

}
