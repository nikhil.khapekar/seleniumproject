package SeleniumObjectRepo;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SelectPaymentPage {
	

	WebDriver driver;


	By lnkSelectDebitOrCreditcard = By.xpath("//div[text()='Credit/Debit Card']");
   // By selectPaymentText=By.xpath("//p[text()='Select Payment']");
    
    


    public SelectPaymentPage(WebDriver driver){

        this.driver = driver;

    }

    public void verifySelectDebitOrCreditCardLink() {
    	assertEquals(driver.findElement(lnkSelectDebitOrCreditcard).isDisplayed(), true, "check Select Debit Or Credit Card Link");
    }
    
    public void clickOnCreditCardLink() {
    	driver.findElement(lnkSelectDebitOrCreditcard).click();
    }


}
