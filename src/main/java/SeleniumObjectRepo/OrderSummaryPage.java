package SeleniumObjectRepo;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OrderSummaryPage {
	
	
	WebDriver driver;


	By btnContinue = By.xpath("//a[contains(@href,'payment')]");
   // By logoCocoStore=By.xpath("//h1[text()='COCO STORE']");
    
    


    public OrderSummaryPage(WebDriver driver){

        this.driver = driver;

    }
    
    public void switchToFrame() {
    	driver.switchTo().frame("snap-midtrans");
    }
    
    
    public void verifyContinueBtn() {
    	assertEquals(driver.findElement(btnContinue).isDisplayed(), true, "check Continue Button");
    }

    
    
    public void clickOnContinueBtn() {
    	
    	driver.findElement(btnContinue).click();
    }


}
