package SeleniumObjectRepo;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	WebDriver driver;

    By btnBuy = By.xpath("//a[text()='BUY NOW']");

    By btnCheckOut = By.xpath("//div[text()='CHECKOUT']");
    
    By btnCancel = By.className("cancel-btn");
        
    By logoCocoStore=By.xpath("//h1[text()='COCO STORE']");
    
    


    public HomePage(WebDriver driver){

        this.driver = driver;

    }
     
    public void WaitForElementToLoad() {
    	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
    
    
    public void verifyCheckOutBtn() {
    //	new WebDriverWait(driver, 30L).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(btnCheckOut));
    	assertEquals(driver.findElement(btnCheckOut).isDisplayed(), true, "check CheckOut Button");
    }
    
    public void verifyBuyBtn() {
    //	new WebDriverWait(driver, 30L).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(btnCheckOut));
    	assertEquals(driver.findElement(btnBuy).isDisplayed(), true, "check Buy Button");
    }

  
    
    public void clickOnBuyBtn() {
    	driver.findElement(btnBuy).click();
    }
    
    public void clickOnCheckOutBtn() {
    	driver.findElement(btnCheckOut).click();
    }

}
